# @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
# A simple makefile to manage this project


GOROOT := /home/debian/debian-package/go/go
GOPATH := /home/debian/debian-package/go-modules



all: rmmaingo configuration build-cxx build build-static 
	

# to check that the system all needed components
configuration:
	@./configure

# absolete (not supported)
ini: 
	export GOROOT=/home/debian/debian-package/go/go
	export PATH=$(PATH):$(GOROOT)/bin


# to compile c++ code before main.go will be substituted
build-cxx:
	-@[ ! -f cxxtools/src/.libs/libcxxtools.a ] && (cd  cxxtools; ./configure --enable-static --disable-shared; make; echo "done"); # to build cxxtools
	@./build.sh  # to build the C++ code in the src/

	
# build go binary with dynamic linking
build: main.go
	@export GOROOT=$(GOROOT); find ./ -iname "*go"|  xargs -I {}  $(GOROOT)/bin/gofmt -s -w {}
	@export GOROOT=$(GOROOT); export GOPATH=$(GOPATH); find `pwd` -iname "main*go" |  xargs -I {} echo "cd \`dirname {}\`; $(GOROOT)/bin/go  build -o \`basename \\\`dirname {}\\\`\`" | bash



# build go binary with statical linking
build-static: main.go
	@export GOROOT=$(GOROOT); find ./ -iname "*go"|  xargs -I {}  $(GOROOT)/bin/gofmt -s -w {}
	@export GOROOT=$(GOROOT); export GOPATH=$(GOPATH); find `pwd` -iname "main*go" |  xargs -I {} echo "cd \`dirname {}\`; $(GOROOT)/bin/go  build --ldflags '-extldflags "-static"'  -o \`basename \\\`dirname {}\\\`\`-static" | bash



# to remove main.go 
rmmaingo:
	-@rm main.go 2> /dev/null # should be produced from template, remove obtained go code

# to clean all temporary stuff
clean: rmmaingo
	@find . -type f -executable -print | grep -v .sh  | grep -v cxxtools | grep -v configure | grep -v .h | xargs -I {} rm {}
	@./clean.sh
	@cd cxxtools ; make clean
	-@rm -r config.log autom4te.cache

# to create a Readme.md
readme:
	 @export GOROOT=$(GOROOT); export GOPATH=$(GOPATH); find `pwd` -iname "doc*go" |  xargs -I {} echo "cd \`dirname {}\`; $(GOPATH)/bin/godocdown  -output=README.md" | bash


# a rule to get main.go from the template
%.go: %.go.template	
	@CGOCFLAGS=`./config.sh --inc`; CGOLDFLAGS=`./config.sh --libs`; cat $^ | sed -e "s#%CGO_CFLAGS%#$$CGOCFLAGS#g" -e "s#%CGO_LDFLAGS%#$$CGOLDFLAGS#g" > $@
	


.PHONY: configuration build-cxx build clean all ini readme build-static rmmaingo
