// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

// This is the documentation of the go-cpp
//
// Introduction
//
// go-cpp is a simple package illustrating basic features of the binding c++ and go codes.
//
//
// To view this godoc on your own machine, just run
//
//   go get code.google.com/p/go.tools/cmd/godoc
//   go get -u bitbucket.org/iggy_floyd/go-cpp/  #(this repo is made to be public, to allow such "go-getting" )
//   godoc -http=:8080
//
// Then you can open documentation in the browser:
//
//  http://localhost:8080/pkg/bitbucket.org/iggy_floyd/go-cpp/
//
// You can clone the git repo
//
//     git clone https://iggy_floyd@bitbucket.org/iggy_floyd/go-cpp.git
//
// Afterwards, compile and execute
//
//    make
//    ./go-cpp
//    ./go-cpp-static
//
//
// As an example, 'hello' (a purely go code), 'print' (cgo, a mixture of c/go codes) and 'Foo' (a C++ class, defined in a 'include/' and 'src/' folders) sub-packages are combined and compiled in one
// go 'go-cpp' or go-cpp-static  binary.
//
// Also the package shows a way to create a statically linked binary from C++ source. The aim of this example is to illustrate how to
// compile and link  any C++ code to the fully statically-linked binary, which will run on any 32- and 64-bits machines under Linux or OSX.
//
//
// Another insteresting example, realized in the package, is linking and use (in the go code) of the library 'cxxtools' which provides a lot of usefull C++ utilities.
// The source code of the 'cxxtools' was taken and adopted from
//
//     git clone https://github.com/maekitalo/cxxtools
//
//
// The package also demonstrates a simple 'configuration' framework which is based the 'configure.ac' autoconf-script. 
// One can add any wanted m4 macro to the 'configure.ac' and put any macro files to m4/ to adopt the 'configuration' checks for own
// purpose. Then simply run 
//
//    autoreconf -ifv -I m4/
//
// to set up the  'configuration' framework of the package.
//
// Here is a list of open publications used to prepare this package:
//
//  http://stackoverflow.com/questions/1713214/how-to-use-c-in-go/1721230  ( an example of the C++ embedding in Go w/o SWING
//  https://code.google.com/p/gosqlite/source/browse/#hg%2Fsqlite%253Fstate%253Dclosed  (SQlite embedding in Go)
//  https://code.google.com/p/gosqlite/source/browse/sqlite/sqlite.go (--//--)
//  http://blog.golang.org/c-go-cgo (an official blog on the topic)
//  https://code.google.com/p/go-wiki/wiki/GoForCPPProgrammers ( CPP to Go datatypes conversions)
//  http://madewithdrew.com/blog/statically-linking-c-to-go/  (statically linking of CGO programs)
//  http://www.trilithium.com/johan/2005/06/static-libstdc/  (--//--)
//  http://stackoverflow.com/questions/18698059/gcc-undefined-reference-to-stdios-baseinitinit (statically linking stdlib of C++ in gcc)
//
//
package main
