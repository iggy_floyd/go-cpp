#!/bin/bash

  program=`basename \`pwd\``  


  for i in src/*.cc; do g++ -fPIC -std=c++0x  -g -Iinclude -Icxxtools/include/  -c ${i}; done
  ar cru lib${program}.a *.o
  ranlib lib${program}.a
  [ ! -d lib ] && mkdir lib
  mv  lib${program}.a lib
  rm *.o 

