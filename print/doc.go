// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

// hello documentation

// The package illustrates simple 'C' function to print the text to std::cout.
// It doesn't require gcc but it uses cgo.
package print
