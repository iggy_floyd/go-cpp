# print
--
    import "."

The package illustrates simple 'C' function to print the text to std::cout. It
doesn't require gcc but it uses cgo.

## Usage

#### func  Print

```go
func Print(s string)
```
Print prints s
