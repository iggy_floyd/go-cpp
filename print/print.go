// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package print

// #include <stdio.h>
// #include <stdlib.h>
import "C"
import "unsafe"

// Print prints s
func Print(s string) {
	cs := C.CString(s)
	C.fputs(cs, (*C.FILE)(C.stdout))
	C.free(unsafe.Pointer(cs))
}
