# hello
--
    import "."

illustrates simple 'hello world' application

## Usage

#### func  Hello

```go
func Hello()
```
Hello prints "hello world"
