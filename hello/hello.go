// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

package hello

import "fmt"

// Hello prints "hello world"
func Hello() {
	fmt.Printf("hello, world\n")
}
