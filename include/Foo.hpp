/* 
 * File:   Foo.hpp
 * Author: debian
 *
 * Created on January 13, 2015, 11:43 AM
 */

#ifndef FOO_HPP
#define	FOO_HPP

#include <iostream>

class cppFoo {
    public:

    
        cppFoo(int _a):a(_a){std::cout<<"Creating Foo object and initializing with "<< a <<"\n";};
        ~cppFoo(){ std::cout<<"Deleting Foo object\n";  };
        void Bar();
    private:
        int a;
};

#endif	/* FOO_HPP */

