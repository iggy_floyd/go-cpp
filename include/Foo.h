/* 
 * File:   Foo.h
 * Author: debian
 *
 * Created on January 13, 2015, 12:24 PM
 */

#ifndef FOO_H
#define	FOO_H

#ifdef	__cplusplus
extern "C" {
#endif

  typedef void* Foo;
  Foo  FooInit(int);
  void FooFree(Foo);
  void FooBar(Foo);


#ifdef	__cplusplus
}
#endif

#endif	/* FOO_H */

