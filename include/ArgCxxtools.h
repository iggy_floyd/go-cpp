/* 
 * File:   ArgCxxtools.h
 * Author: debian
 *
 * Created on January 13, 2015, 3:37 PM
 */

#ifndef ARGCXXTOOLS_H
#define	ARGCXXTOOLS_H

#ifdef	__cplusplus
extern "C" {
#endif

  typedef void* Argcxxtools;
  //Argcxxtools  ArgcxxtoolsInit(int argc, char *argv[], char * option);
  Argcxxtools  ArgcxxtoolsInit(int argc, char *argv[], char option);
  void ArgcxxtoolsFree(Argcxxtools);
  void PrintArg(Argcxxtools);



#ifdef	__cplusplus
}
#endif

#endif	/* ARGCXXTOOLS_H */

