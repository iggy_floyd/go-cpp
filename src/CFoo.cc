#include "Foo.hpp"
#include "Foo.h"


Foo FooInit(int a )
{
  cppFoo * ret = new cppFoo(a);
  return (void*)ret;
}
void FooFree(Foo f)
{
  cppFoo * foo = (cppFoo*)f;
  delete foo;
}
void FooBar(Foo f)
{
  cppFoo * foo = (cppFoo*)f;
  foo->Bar();
}