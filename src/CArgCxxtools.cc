#include "ArgCxxtools.h" 
#include <string>
#include <iostream>
#include <cxxtools/arg.h>




Argcxxtools ArgcxxtoolsInit(int argc, char *argv[], char  option)
{
  std::cout<<" Create cxxtools::Arg with: \n";
  std::cout<<" option: "<<option<<"\n";
  std::cout<<" arg[0]: "<<argv[0]<<"\n";
  std::cout<<" arg[1]: "<<argv[1]<<"\n";
  std::cout<<" arg[2]: "<<argv[1]<<"\n";
  std::cout<<" argc: "<<argc<<"\n";
  cxxtools::Arg<std::string> * arg = new cxxtools::Arg<std::string>  (argc, argv, option,"UNKNOWN");   
  std::cout<<"Argument is initialized to "<<arg->getValue()<<"\n";
  return (Argcxxtools)arg;
}



void ArgcxxtoolsFree(Argcxxtools f)
{
  cxxtools::Arg<std::string> *  arg = (cxxtools::Arg<std::string> *)f;
  std::cout<<"Delete cxxtools::Arg of value "<< arg->getValue() <<"\n";
  delete arg;
}



void PrintArg(Argcxxtools f) {
     cxxtools::Arg<std::string> *  arg = (cxxtools::Arg<std::string> *)f;

        std::cout<<"cxxtools::Arg is set to "<<arg->getValue()<<"\n"; 
}